#!/bin/bash

# See https://solarianprogrammer.com/2018/05/06/building-gcc-cross-compiler-raspberry-pi/ for a pretty nice description on the steps required for building a cross compiler

# stop script on first error
set -e

mkdir gcc_for_raspberrypi
cd gcc_for_raspberrypi

# variables that define the result
install_dir="$(pwd)/install"
arch=armv6
tune=arm1176jzf-s # since our target device is an pi zero w

binutils_version='2.31' # SHOULD match that of your RPI; check with 'ld -v' 
glibc_version='2.28' # MUST match that of your RPI; check with 'ldd --version'
gcc_version='8.3.0' # If you are trying to build Glibc 2.28 with a more modern GCC, like 9.x or 10.x, you are going to get a lot of errors.
# ^- check with 'gcc --version'

export PATH=/$install_dir/bin:$PATH

# get sources
curl -Lo binutils.tar.bz2 \
  "https://ftpmirror.gnu.org/binutils/binutils-$binutils_version.tar.bz2"
curl -Lo gcc.tar.xz \
  "https://ftpmirror.gnu.org/gcc/gcc-$gcc_version/gcc-$gcc_version.tar.xz"
curl -Lo glibc.tar.bz2 \
  "https://ftpmirror.gnu.org/glibc/glibc-$glibc_version.tar.bz2"
git clone --depth=1 https://github.com/raspberrypi/linux

#
cd linux
KERNEL=kernel7
make ARCH=arm INSTALL_HDR_PATH="$install_dir/arm-linux-gnueabihf" headers_install
cd ..

# build binutils
mkdir binutils_source
cd binutils_source
tar --strip-components 1 -xf ../binutils.tar.bz2
mkdir ../binutils_build
cd ../binutils_build
../binutils_source/configure \
  --prefix="$install_dir" \
  --target=arm-linux-gnueabihf \
  --with-arch=$arch \
  --with-tune=$tune \
  --with-fpu=vfp \
  --with-float=hard \
  --disable-multilib \
  --enable-gold=yes \
  --enable-lto
make -j$(nproc)
make install
cd ..

# partially build gcc
mkdir gcc_source
cd gcc_source
tar --strip-components 1 -xf ../gcc.tar.xz
mkdir ../gcc_build
cd ../gcc_build
# contrib/download_prerequisites
../gcc_source/configure \
  --prefix="$install_dir" \
  --target=arm-linux-gnueabihf \
  --with-arch=$arch \
  --with-tune=$tune \
  --with-fpu=vfp \
  --with-float=hard \
  --disable-multilib \
  --enable-languages=c,c++,go
make -j$(nproc) all-gcc
make install-gcc
cd ..

# partially build Glibc:
mkdir glibc_source
cd glibc_source
tar --strip-components 1 -xf ../glibc.tar.bz2
mkdir ../glibc_build
cd ../glibc_build
../glibc_source/configure --prefix="$install_dir/arm-linux-gnueabihf" --build=$MACHTYPE --host=arm-linux-gnueabihf --target=arm-linux-gnueabihf --with-arch=armv6 --with-fpu=vfp --with-float=hard --with-headers="$install_dir/arm-linux-gnueabihf/include" --disable-multilib libc_cv_forced_unwind=yes
make install-bootstrap-headers=yes install-headers
make -j$(nproc) csu/subdir_lib
install csu/crt1.o csu/crti.o csu/crtn.o "$install_dir/arm-linux-gnueabihf/lib"
arm-linux-gnueabihf-gcc -nostdlib -nostartfiles -shared -x c /dev/null -o "$install_dir/arm-linux-gnueabihf/lib/libc.so"
touch "$install_dir/arm-linux-gnueabihf/include/gnu/stubs.h"

# back to GCC
cd ../gcc_build
make -j$(nproc) all-target-libgcc
make install-target-libgcc

# finish building Glibc
cd ../glibc_build
make -j$(nproc)
make install

# finish building GCCs

cd ../gcc_build
make -j$(nproc)
make install

cd ..
echo "Successfully compiled 'binutils' and 'gcc'"

# result should be in ./gcc_for_raspberrypi/install/bin/ (arm-linux-gnueabihf-gcc for the c-compiler, as an example)