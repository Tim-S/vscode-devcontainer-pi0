# vscode-devcontainer-pi0

If you find yourself in the situation where you need to cross-compile for an raspberry pi zero (w) for reducing the build time
and do not have a ready-to-go binary at hand feel free to build your own cross compiler with the dockerfile provied.

Yet this project provides only a Dockerfile to create the crosscompiler, but the objective is to extend this with a vscode-devcontainer configuration
with options automatically transfer build executables to the pi.
`

## Build the compiler

```sh
docker build -t schneidertim/create-rpi0-gcc-crosscompiler .
```

Afterwards a successful build, you can find the cross-compile-binaries in /scripts/gcc_for_raspberrypi/install/bin/  (arm-linux-gnueabihf-gcc for the c-compiler, as an example)

```powershell
docker run -it --rm -v "${PWD}:/workspace" -w /workspace schneidertim/create-rpi0-gcc-crosscompiler
```

```bash
/scripts/gcc_for_raspberrypi/install/bin/arm-linux-gnueabihf-g++ test.cpp -o test
```

apt install -y cmake rsync ssh
ssh pi@192.168.2.201

scp -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null test pi@192.168.2.201:
ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null pi@192.168.2.201 "./test \"Hello World\""

echo $?

```bash
export CMAKE_TOOLCHAIN_FILE=${PWD}/cmake/TC-arm-linux-gnueabihf.cmake
export CMAKE_EXPORT_COMPILE_COMMANDS=On
cmake -B build -S test -D CMAKE_TOOLCHAIN_FILE=$CMAKE_TOOLCHAIN_FILE
cmake --build build

scp -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null build/test-arm-toolchain pi@192.168.2.201:
ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null pi@192.168.2.201 "./test-arm-toolchain \"Hello World\""
```

<https://deardevices.com/2019/12/25/raspberry-pi-sysroot/>

## copy SYSROOT from pi
```bash
mkdir sysroot
cd sysroot

rsync -avz pi0:/lib .
rsync -avz pi0:/usr/include usr
rsync -avz pi0:/usr/lib usr
```

## Remote Debugging
```bash
local$ ssh -L9091:localhost:9091 pi0
remote$ gdbserver --multi :9091
```

```bash
gdb-multiarch\
  -ex 'target extended-remote <ip>:<port>'\
  -ex 'set remote exec-file <remote-path to binary>'\
  -ex 'set substitute-path <map-this> <to-here>'"
  <binary path>
```