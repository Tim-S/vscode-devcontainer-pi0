set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_SYSTEM_PROCESSOR arm)

set(tools /scripts/gcc_for_raspberrypi/install)
# set(rootfs_dir /workspace/sysroot)#
if(NOT DEFINED rootfs_dir)
  get_filename_component(rootfs_dir ${CMAKE_CURRENT_LIST_DIR}/../sysroot ABSOLUTE)
endif(NOT DEFINED rootfs_dir)

SET(CMAKE_FIND_ROOT_PATH ${rootfs_dir})
SET(CMAKE_SYSROOT ${rootfs_dir})
# set(CMAKE_STAGING_PREFIX /home/devel/stage)

message(STATUS "CMAKE_SYSROOT: ${CMAKE_SYSROOT}")

set(CMAKE_C_COMPILER ${tools}/bin/arm-linux-gnueabihf-gcc)
set(CMAKE_CXX_COMPILER ${tools}/bin/arm-linux-gnueabihf-g++)

set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)