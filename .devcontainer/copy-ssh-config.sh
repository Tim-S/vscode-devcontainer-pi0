#!/bin/sh
set -e

# Using `-v $HOME/.ssh:/root/.ssh:ro` produce permissions error while in the container
# to prevent that we ensure that the `-v $HOME/.ssh:/tmp/.ssh:ro` as the correct
# permissions when starting the container - @awea 20200319
# Source: https://nickjanetakis.com/blog/docker-tip-56-volume-mounting-ssh-keys-into-a-docker-container
cp -R /tmp/.ssh /root/.ssh
chmod 700 /root/.ssh
chmod 644 /root/.ssh/id_rsa.pub
chmod 600 /root/.ssh/id_rsa

exec "$@"